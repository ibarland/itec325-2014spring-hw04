<?php require_once('utils.php'); ?>

<html>
  <head>
    <title>hw04 pictures</title>
  </head>
  <body>

  <p>
    Here are pictures submitted by various students for hw04 / git practice.
    <ul>
      <li>
	Before the ravages of time gave me significant grey:
         <?php echo thumbnail('ibarland-dept-portrait-2006-Jan.jpg', 100); ?>
      </li>
      <li>
        A yellow spotted rock 
        <?php
            echo hyperlink('https://en.wikipedia.org/wiki/Hyrax', 'hyrax:'), '<br />'; 
            echo thumbnail('yellow-spotted-rock-hyrax.jpg', 100);
        ?>
      </li>

      <li>
	Database humor
	<br />
	<?php
	    echo thumbnail('dbhumor.jpg', 500);
        ?>
      </li>
      <li>
        FACT: Bears eat beets.
        <?php
            echo hyperlink('http://wiki.answers.com/Q/Do_bears_eat_beets', 'Yes, they do.'), '<br />';
            echo thumbnail('schrute.jpg', 100);
        ?>
      </li>
      <li>
        Fahrvergnugen
        <?php
            echo hyperlink('http://imgur.com/pzmFBRL','Fahrvergnugen'), '<br />';
            echo thumbnail('volkswagen.jpg', 100);
         ?>
      </li>
	  <li>
	    Being a nerd
		<?php
			echo hyperlink('http://en.wikipedia.org/wiki/Nerd','Nerd'), '<br />';
			echo thumbnail('aEveQzx_460s.jpg', 100);
			?>
		</li>
      <li>
        The greatest radio station in the world
        <?php
          echo hyperlink('http://wuvt.vt.edu', 'WUVT'), '<br />';
          echo thumbnail('wuvt.jpg', 100);
        ?>
      </li>
	  <li>
        On top of Buffalo
		<?php
		 echo hyperlink('http://imgur.com/MOst6aV', 'Buffalo'), '<br />';
          echo thumbnail('buffalo.jpg', 100);
		?>
       <li>
         My Portfolio

        <?php

          echo hyperlink('http://purulentsoftware.com/portfolio/portfolio.html', 'Portfolio');
          echo '<br />';
          echo thumbnail('cmays2.jpg', 100);
        ?>
      </li>

	  <li>
		<?php echo hyperlink("http://urbanator.deviantart.com/art/Joker-and-Harley-Quinn-Batman-436327941","Joker and Harley Quinn" ), "<br />";
		echo thumbnail('joker_and_harley_quinn.png',100);?>
	  </li>

      <li>
		<?php echo hyperlink("http://ecologyadventure2.edublogs.org/files/2011/04/canadian-lynx.jpg-2-2e8qx7n.jpg","Canadian Lynx" ), "<br />";
		echo thumbnail('canadian_lynx.jpg',100);?>
	  </li>

    <li>
    <?php echo hyperlink("http://en.wikipedia.org/wiki/File:Flip-Flops_socks.jpg" ), "<br />";
    echo thumbnail('socks.jpg',100);?>
    </li>
	  
	  <li>
         Sandy Cheeks
        <?php

          echo hyperlink('http://i1344.photobucket.com/albums/p650/megsalva/IMG_9904_zpsf0eb6b69.jpg', 'Sandy Cheeks');
          echo thumbnail('sandy.jpg', 100);
        ?>
      </li>
    </ul>
  </p>

  <p>
    Do NOT add any php code which is attacks php.radford.edu via a denial-of-service-attack,
    inadvertent or not.
    (Don't attack any other sites, either.)
  </p>



  </body>
</html>
